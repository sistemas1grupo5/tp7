package Compilar;

import java.io.IOError;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class AnalizadorSemantico {
    Logger LOG = Logger.getLogger(AnalizadorSemantico.class.getName());
    
    private final IndicadorDeErrores indicadorDeErrores;
    private final IdentificadorBean[] tabla;
    private Integer count;
    

    public AnalizadorSemantico(IndicadorDeErrores indicadorDeErrores) {
        this.indicadorDeErrores = indicadorDeErrores;
        this.tabla = new IdentificadorBean[Constantes.MAX_CANT_IDENT];
        this.count = 0;
    }  
   
    public IdentificadorBean retroceder (int max, String identificador){
        for(int i=max; i>=0; i--){
            if (this.tabla[i].getNombre().equals(identificador)) {
                return this.tabla[i];
            }
        }
        return null;
    }

    //Avanzar
    public IdentificadorBean avanzar ( int min, int max, String identificador){
        for(int i=min; i<=max; i++){            
            //LOG.log(Level.INFO, "compare " + this.tabla[i].getNombre() + " " + identificador);
            if (this.tabla[i].getNombre().equals(identificador)) {
                return this.tabla[i];
            }    
        }
        return null;
    }
    
    //insertar nuevo Identificador
    public IdentificadorBean insertarIdentificador(int posicion, String identificador, Terminal t) throws IOException
    {
        IdentificadorBean id;
        id = new IdentificadorBean();
        id.setNombre(identificador);
        id.setTipo(t);
        id.setValor(0);
        this.tabla[posicion] = id; 
        count++;
        return id;
    }
    
    public Integer getCount() {
        return count;
    }
    
    public Integer getOffset() {
        return (count - 1) * 4;
    }
    

}
