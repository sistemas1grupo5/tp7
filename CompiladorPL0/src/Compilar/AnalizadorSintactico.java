package Compilar;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class AnalizadorSintactico {

    private static final Logger logger = Logger.getLogger(AnalizadorSintactico.class.getName());

    private final AnalizadorLexico aLex;
    private final AnalizadorSemantico aSem;
    private final GeneradorDeCodigo genCod;
    private final IndicadorDeErrores indicadorDeErrores;

    public AnalizadorSintactico(AnalizadorLexico aLex, AnalizadorSemantico aSem, GeneradorDeCodigo genCod, IndicadorDeErrores indicadorDeErrores) {
        this.aLex = aLex;
        this.aSem = aSem;
        this.genCod = genCod;
        this.indicadorDeErrores = indicadorDeErrores;
        logger.log(Level.INFO, "se instancio");
    }

    public void analizar() throws IOException {
        aLex.escanear();

        programa();
        genCod.volcar();
    }

    private void programa() throws IOException {
        int programSize, variablesPosition, sizeText, sizeRawText, sectionAlignment, size;

        genCod.cargarAlFinal(0xbf);  //index to variables section
        variablesPosition = genCod.getTopeMemoria();
        genCod.cargar4Bytes(0);

        programSize = (genCod.getTopeMemoria());
        logger.log(Level.INFO, "empezo programa");
        bloque(0);

        if (aLex.getS() == Terminal.PUNTO) {
            aLex.escanear();
            logger.log(Level.INFO, "Termino programa");
            genCod.cargarAlFinal(0xE9);  //jump to end of program "routine"
            programSize = (genCod.getTopeMemoria() + 4);
            genCod.cargar4Bytes(0x588 - programSize); //end end of program

            genCod.cargar4BytesEn(variablesPosition, 0x401500 + genCod.getTopeMemoria() - 0x700);//fixup index to variable section

            for (int i = 0; i < aSem.getCount(); i++) { //Set Identificadores vaule
                //logger.log(Level.INFO, "Insertando identificafor nro " + i);
                genCod.cargar4Bytes(0);
            }

            sizeText = genCod.getTopeMemoria() - 0x200; // set header field text size
            logger.log(Level.INFO, "Sixze of text " + sizeText);
            genCod.cargar4BytesEn(416, sizeText);

            int fileAlignment = genCod.get4BytesValue(220); //add 0, make file size multiple of 
            while (genCod.getTopeMemoria() % fileAlignment != 0) { //header field "File alignment"
                genCod.cargarAlFinal(0);
            }

            sizeRawText = genCod.getTopeMemoria() - 0x200;

            genCod.cargar4BytesEn(424, sizeRawText); //header field "Size of raw data"
            genCod.cargar4BytesEn(188, sizeRawText); //header field "Size of code"
            logger.log(Level.INFO, "Size of text " + sizeText);

            sectionAlignment = genCod.get4BytesValue(216);
            size = (2 + sizeRawText / sectionAlignment) * sectionAlignment;
            genCod.cargar4BytesEn(208, size);
            genCod.cargar4BytesEn(240, size);

        } else {
            // terminar las demas funciones primero.
            indicadorDeErrores.mostrar(1, "", aLex.getLinea(), aLex.getColumna());
        }
    }

    private void bloque(int base) throws IOException {
        int beforeJump, afterJump, jump;
        int desplazamiento = 0; //arranca en 0 cada vez que llamamos a bloque.
        String identificador;
        IdentificadorBean id;

        beforeJump = genCod.getTopeMemoria();
        genCod.cargarAlFinal(0xe9);
        genCod.cargar4Bytes(0); //fix-up later
        afterJump = genCod.getTopeMemoria();

        if (aLex.getS() == Terminal.CONST) {
            do {
                aLex.escanear();
                if (aLex.getS() == Terminal.IDENTIFICADOR) {
                    identificador = aLex.getCad();
                    if (identificador.length() > Constantes.MAX_LONG_IDENT) {
                        indicadorDeErrores.mostrar(22, identificador, aLex.getLinea(), aLex.getColumna());
                    }
                    logger.log(Level.INFO, "se encontro constante identificador " + identificador);
                    if (aSem.avanzar(base, desplazamiento - 1, identificador) != null) {
                        indicadorDeErrores.mostrar(13, identificador, aLex.getLinea(), aLex.getColumna());
                        logger.log(Level.SEVERE, "se encontro DUPLICADA constante identificador " + identificador);
                        System.exit(1);
                    }
                    aLex.escanear();
                    if (aLex.getS() == Terminal.IGUAL) {
                        aLex.escanear();
                        if (aLex.getS() == Terminal.NUMERO) {
                            id = aSem.insertarIdentificador(base + desplazamiento, identificador, Terminal.CONST);
                            id.setValor(Integer.parseInt(aLex.getCad()));
                            desplazamiento++;
                        } else {
                            indicadorDeErrores.mostrar(4, aLex.getCad(), aLex.getLinea(), aLex.getColumna());
                        }
                    } else {
                        indicadorDeErrores.mostrar(3, "", aLex.getLinea(), aLex.getColumna());
                    }
                } else {
                    indicadorDeErrores.mostrar(2, aLex.getCad(), aLex.getLinea(), aLex.getColumna());
                }
                aLex.escanear();
            } while (aLex.getS() == Terminal.COMA);
            if (aLex.getS() == Terminal.PUNTO_Y_COMA) {
                aLex.escanear();
            } else {
                indicadorDeErrores.mostrar(5, "", aLex.getLinea(), aLex.getColumna());
            }
        }

        if (aLex.getS() == Terminal.VAR) {
            do {
                aLex.escanear();
                if (aLex.getS() == Terminal.IDENTIFICADOR) {
                    identificador = (String) aLex.getCad();
                    if (identificador.length() > Constantes.MAX_LONG_IDENT) {
                        indicadorDeErrores.mostrar(22, identificador, aLex.getLinea(), aLex.getColumna());
                    }
                    logger.log(Level.INFO, "Se encontró (variable) identificador " + identificador + " " + aLex.getLinea() + "," + aLex.getColumna());
                    if (aSem.avanzar(base, base + desplazamiento - 1, aLex.getCad()) != null) {
                        //errror identificador duplicado
                        indicadorDeErrores.mostrar(16, identificador, aLex.getLinea(), aLex.getColumna());
                    } else {
                        logger.log(Level.INFO, "posicion variable " + identificador + " " + (base + desplazamiento));
                        id = aSem.insertarIdentificador(base + desplazamiento, identificador, Terminal.VAR);
                        id.setValor(aSem.getOffset());
                        desplazamiento++;
                    }
                    aLex.escanear();
                    //logger.log(Level.INFO, "Se encontró " + aLex.getS() + " " + aLex.getLinea() + "," + aLex.getColumna());
                }

            } while (aLex.getS() == Terminal.COMA);

        }
        if (aLex.getS() == Terminal.PUNTO_Y_COMA) {
            aLex.escanear();
        }

        while (aLex.getS() == Terminal.PROCEDURE) {
            logger.log(Level.INFO, "Se encontró " + aLex.getS() + " " + aLex.getLinea() + "," + aLex.getColumna());
            aLex.escanear();

            if (aLex.getS() == Terminal.IDENTIFICADOR) {
                identificador = aLex.getCad();
                logger.log(Level.INFO, "Se encontró Identificador de procedimiento " + identificador + " " + aLex.getLinea() + "," + aLex.getColumna());
                if (aSem.avanzar(base, desplazamiento - 1, identificador) != null) {
                    indicadorDeErrores.mostrar(16, identificador, aLex.getLinea(), aLex.getColumna());
                }
                id = aSem.insertarIdentificador(base + desplazamiento, identificador, Terminal.PROCEDURE);
                id.setValor(genCod.getTopeMemoria());
                desplazamiento++;
                aLex.escanear();
            } else {
                indicadorDeErrores.mostrar(2, "", aLex.getLinea(), aLex.getColumna());
            }

            if (aLex.getS() == Terminal.PUNTO_Y_COMA) {
                logger.log(Level.INFO, "Se encontró " + aLex.getS() + " " + aLex.getLinea() + "," + aLex.getColumna());
                aLex.escanear();
            } else {
                indicadorDeErrores.mostrar(3, "", aLex.getLinea(), aLex.getColumna());
            }

            bloque(base + desplazamiento);
            genCod.cargarAlFinal(0xc3); // return
            if (aLex.getS() == Terminal.PUNTO_Y_COMA) {
                logger.log(Level.INFO, "Se encontró " + aLex.getS() + " " + aLex.getLinea() + "," + aLex.getColumna());

                aLex.escanear();
            } else {
                indicadorDeErrores.mostrar(3, "", aLex.getLinea(), aLex.getColumna());
            }
        }

        jump = genCod.getTopeMemoria() - afterJump;
        genCod.cargar4BytesEn(afterJump - 4, jump); //fix-up bloque 
        logger.log(Level.INFO, "Bloque  posicion " + genCod.getTopeMemoria() + " comienzo " + beforeJump + " salto " + jump);

        proposicion(base, desplazamiento);

    }

    private void proposicion(int base, int desplazamiento) throws IOException {
        String identificador, cadena;
        IdentificadorBean id;
        int nextFree, beforeCondition, beforeJump, jump;

        switch (aLex.getS()) {
            case IDENTIFICADOR:
                identificador = (String) aLex.getCad();
                logger.log(Level.INFO, "Se encotró identificador: " + identificador);
                id = aSem.retroceder(base + desplazamiento - 1, identificador);
                if (id == null) {
                    indicadorDeErrores.mostrar(17, " se encontro " + aLex.getCad(), aLex.getLinea(), aLex.getColumna());
                }
                if (id.getTipo() != Terminal.VAR) {
                    indicadorDeErrores.mostrar(19, id.getTipo().toString(), aLex.getLinea(), aLex.getColumna());
                }

                aLex.escanear();
                if (aLex.getS() != Terminal.ASIGNACION) {
                    indicadorDeErrores.mostrar(6, " se encontro " + aLex.getCad(), aLex.getLinea(), aLex.getColumna());
                }
                logger.log(Level.INFO, "Se encotró : " + aLex.getCad());
                aLex.escanear();
                expresion(base, desplazamiento);
                genCod.insertAssignVar(id);

                break;

            case CALL:
                aLex.escanear();
                if (aLex.getS() != Terminal.IDENTIFICADOR) {
                    indicadorDeErrores.mostrar(2, " se encontro " + aLex.getCad(), aLex.getLinea(), aLex.getColumna());
                }
                identificador = aLex.getCad();
                id = aSem.retroceder(base + desplazamiento - 1, identificador);
                if (id == null) {
                    indicadorDeErrores.mostrar(17, aLex.getCad(), aLex.getLinea(), aLex.getColumna());
                }
                if (id.getTipo() != Terminal.PROCEDURE) {
                    indicadorDeErrores.mostrar(19, id.getNombre() + " tipo: " + id.getTipo().toString(), aLex.getLinea(), aLex.getColumna());
                }
                LOG.log(Level.INFO, "procedure " + identificador + " posicion procedure " + id.getValor() + " posicion actual " + genCod.getTopeMemoria());
                genCod.insertCall(id.getValor() - genCod.getTopeMemoria() - 5);
                aLex.escanear();
                break;

            case BEGIN:
                do {
                    aLex.escanear();
                    proposicion(base, desplazamiento);
                } while (aLex.getS() == Terminal.PUNTO_Y_COMA);
                if (aLex.getS() != Terminal.END) {
                    indicadorDeErrores.mostrar(7, "Se encontro " + aLex.getCad(), aLex.getLinea(), aLex.getColumna());
                }
                aLex.escanear();
                break;
            case IF:
                aLex.escanear();
                beforeCondition = genCod.getTopeMemoria();
                condicion(base, desplazamiento);
                beforeJump = genCod.getTopeMemoria();
                genCod.insertJump(0x0); //fix-up later
                if (aLex.getS() != Terminal.THEN) {
                    indicadorDeErrores.mostrar(8, "Se encontro " + aLex.getCad(), aLex.getLinea(), aLex.getColumna());
                }
                aLex.escanear();
                proposicion(base, desplazamiento);
                jump = genCod.getTopeMemoria() - beforeJump - 5;
                logger.log(Level.INFO, "position start" + beforeCondition + "end: " + genCod.getTopeMemoria() + " diff " + jump);
                genCod.cargar4BytesEn(beforeJump + 1, jump); //fix-up

                break;

            case WHILE:
                aLex.escanear();
                beforeCondition = genCod.getTopeMemoria();
                condicion(base, desplazamiento);
                beforeJump = genCod.getTopeMemoria();
                genCod.insertJump(0x0); //fix-up later (jump to end of while)
                if (aLex.getS() != Terminal.DO) {
                    indicadorDeErrores.mostrar(9, "Se encontro " + aLex.getCad(), aLex.getLinea(), aLex.getColumna());
                }
                aLex.escanear();
                proposicion(base, desplazamiento);
                genCod.insertJump(beforeCondition - genCod.getTopeMemoria() - 5); //jump to before condition

                jump = genCod.getTopeMemoria() - beforeJump - 5;
                logger.log(Level.INFO, "while position start " + beforeCondition + " end: " + genCod.getTopeMemoria() + " diff " + jump);
                genCod.cargar4BytesEn(beforeJump + 1, jump); //fix-up

                break;
            case READLN:
                logger.log(Level.INFO, "Se encotró (readln) : " + aLex.getCad());
                aLex.escanear();
                if (aLex.getS() == Terminal.ABRE_PARENTESIS) {
                    do {
                        aLex.escanear();
                        identificador = aLex.getCad();
                        id = aSem.retroceder(base + desplazamiento - 1, identificador);
                        if (id == null) {
                            indicadorDeErrores.mostrar(17, identificador, aLex.getLinea(), aLex.getColumna());
                        }
                        if (id.getTipo() != Terminal.VAR) {
                            indicadorDeErrores.mostrar(19, " se esperaba VAR se encontro " + id.getTipo().toString(), aLex.getLinea(), aLex.getColumna());
                        }
                        genCod.insertRead(id.getValor());
                        logger.log(Level.INFO, "Se encotró (readln) identificador : " + aLex.getCad());
                        aLex.escanear();
                    } while (aLex.getS() == Terminal.COMA);
                    if (aLex.getS() != Terminal.CIERRA_PARENTESIS) {
                        indicadorDeErrores.mostrar(14, "Se encontro " + aLex.getCad(), aLex.getLinea(), aLex.getColumna());
                    }
                    aLex.escanear();
                } else {
                    indicadorDeErrores.mostrar(15, "Se encontro " + aLex.getCad(), aLex.getLinea(), aLex.getColumna());
                }
                break;
            case WRITE:
                logger.log(Level.INFO, "Se encotró (write) : " + aLex.getCad());
                aLex.escanear();
                if (aLex.getS() == Terminal.ABRE_PARENTESIS) {
                    do {
                        aLex.escanear();
                        if (aLex.getS() == Terminal.CADENA_LITERAL) {
                            cadena = (String) aLex.getCad();
                            if (cadena.length() > Constantes.MAX_TAM_CADENA) {
                                indicadorDeErrores.mostrar(23, "Se encontro " + aLex.getCad(), aLex.getLinea(), aLex.getColumna());
                            }
                            logger.log(Level.INFO, "Se encotró (write) cadena : " + aLex.getCad());
                            genCod.insertWriteString(aLex.getCad());
                            aLex.escanear();
                        } else {
                            expresion(base, desplazamiento);
                            genCod.insertWrite();
                        }
                    } while (aLex.getS() == Terminal.COMA);
                    if (aLex.getS() != Terminal.CIERRA_PARENTESIS) {
                        indicadorDeErrores.mostrar(14, "Se encontro " + aLex.getCad(), aLex.getLinea(), aLex.getColumna());
                    }
                    aLex.escanear();
                } else {
                    indicadorDeErrores.mostrar(15, "Se encontro " + aLex.getCad(), aLex.getLinea(), aLex.getColumna());
                }
                break;
            case WRITELN:
                logger.log(Level.INFO, "Se encotró (writeln) : " + aLex.getCad());
                aLex.escanear();
                if (aLex.getS() == Terminal.ABRE_PARENTESIS) {
                    do {
                        aLex.escanear();
                        if (aLex.getS() == Terminal.CADENA_LITERAL) {
                            cadena = (String) aLex.getCad();
                            if (cadena.length() > Constantes.MAX_TAM_CADENA) {
                                indicadorDeErrores.mostrar(23, "Se encontro " + aLex.getCad(), aLex.getLinea(), aLex.getColumna());
                            }

                            logger.log(Level.INFO, "Se encotró (writeln) cadena : " + aLex.getCad());
                            genCod.insertWriteString(aLex.getCad());
                            aLex.escanear();
                        } else {
                            expresion(base, desplazamiento);
                            genCod.insertWrite();
                        }
                    } while (aLex.getS() == Terminal.COMA);
                    if (aLex.getS() != Terminal.CIERRA_PARENTESIS) {
                        indicadorDeErrores.mostrar(14, "Se encontro " + aLex.getCad(), aLex.getLinea(), aLex.getColumna());
                    }
                    aLex.escanear();
                }
                genCod.insertWriteLn();
                break;
        }

    }

    private void condicion(int base, int desplazamiento) throws IOException {
        Terminal operator;
        if (aLex.getS() == Terminal.ODD) {
            logger.log(Level.INFO, "Se encotró (condicion): " + aLex.getS());
            aLex.escanear();
            expresion(base, desplazamiento);
            genCod.insertCompareOddJump();
        } else {
            expresion(base, desplazamiento);
            operator = aLex.getS();
            switch (aLex.getS()) {
                case IGUAL:
                    break;
                case DISTINTO:
                    break;
                case MENOR:
                    break;
                case MENOR_IGUAL:
                    break;
                case MAYOR:
                    break;
                case MAYOR_IGUAL:
                    break;
                default:
                    indicadorDeErrores.mostrar(11, "encontro (condicion): " + aLex.getCad(), aLex.getLinea(), aLex.getColumna());
                    break;
            }
            logger.log(Level.INFO, "Se encotró (condicion) : " + aLex.getCad());
            aLex.escanear();
            expresion(base, desplazamiento);
            genCod.insertCompareJump(operator);

        }
    }

    private void expresion(int base, int desplazamiento) throws IOException {
        Terminal operacion;
        logger.log(Level.INFO, "Se encotró (expresion): " + aLex.getCad() + " linea " + aLex.getLinea() + ", col " + aLex.getColumna());
        operacion = null;
        switch (aLex.getS()) {
            case MAS:
                operacion = Terminal.MAS;
                aLex.escanear();
                break;
            case MENOS:
                operacion = Terminal.MENOS;
                aLex.escanear();
                break;
        }

        termino(base, desplazamiento);
        if (operacion != null) {
            if (operacion == Terminal.MAS) {
                //genCod.insertAdd();
            } else {
                genCod.insertLoadEAX(-1);
                genCod.insertPushEAX();
                genCod.insertTimes();
            }
        } else {
        }

        while (aLex.getS() == Terminal.MAS || aLex.getS() == Terminal.MENOS) {
            logger.log(Level.INFO, "Se encotró (expresion): " + aLex.getCad() + " linea " + aLex.getLinea() + ", col " + aLex.getColumna());
            operacion = null;
            switch (aLex.getS()) {
                case MAS:
                    operacion = Terminal.MAS;
                    aLex.escanear();
                    break;
                case MENOS:
                    operacion = Terminal.MENOS;
                    aLex.escanear();
                    break;
            }
            termino(base, desplazamiento);
            if (operacion != null) {
                if (operacion == Terminal.MAS) {
                    genCod.insertAdd();
                } else {
                    genCod.insertSubstract();
                }
            }
        }

    }

    private void termino(int base, int desplazamiento) throws IOException {
        factor(base, desplazamiento);
        Terminal operacion;
        while (aLex.getS() == Terminal.POR || aLex.getS() == Terminal.DIVIDIDO) {
            logger.log(Level.INFO, "Se encotró (termino): " + aLex.getCad() + " linea " + aLex.getLinea() + ", col " + aLex.getColumna());
            operacion = null;
            if (aLex.getS() == Terminal.POR) {
                operacion = aLex.getS();
                aLex.escanear();
                logger.log(Level.INFO, "encotre POR ");
            }
            if (aLex.getS() == Terminal.DIVIDIDO) {
                operacion = aLex.getS();
                aLex.escanear();
                logger.log(Level.INFO, "encotre DIVIDIDO ");
            }
            factor(base, desplazamiento);
            if (operacion != null) {
                if (operacion == Terminal.POR) {
                    genCod.insertTimes();
                } else {
                    genCod.insertDivide();
                }
            }
        }
    }
    private static final Logger LOG = Logger.getLogger(AnalizadorSintactico.class
            .getName());

    private void factor(int base, int desplazamiento) throws IOException {
        IdentificadorBean id;
        switch (aLex.getS()) {
            case IDENTIFICADOR:
                logger.log(Level.INFO, "encotre identificador (factor) " + aLex.getCad() + " linea " + aLex.getLinea() + " col " + aLex.getColumna());
                id = aSem.retroceder(base + desplazamiento - 1, aLex.getCad());
                if (id == null) {
                    indicadorDeErrores.mostrar(17, aLex.getCad(), aLex.getLinea(), aLex.getColumna());
                }
                if (id.getTipo() == Terminal.VAR) {
                    genCod.insertVarToEAX(id);
                } else if (id.getTipo() == Terminal.CONST) {
                    genCod.insertLoadEAX(id.getValor());
                    genCod.insertPushEAX();
                } else {
                    indicadorDeErrores.mostrar(19, aLex.getCad() + " " + id.getTipo().toString(), aLex.getLinea(), aLex.getColumna());
                }
                aLex.escanear();

                break;
            case NUMERO:
                logger.log(Level.INFO, "encotre numero (factor) " + aLex.getCad() + " linea " + aLex.getLinea() + " col " + aLex.getColumna());
                genCod.insertLoadEAX(Integer.parseInt(aLex.getCad()));
                genCod.insertPushEAX();
                aLex.escanear();
                break;
            case ABRE_PARENTESIS:
                aLex.escanear();
                expresion(base, desplazamiento);
                if (aLex.getS() == Terminal.CIERRA_PARENTESIS) {
                    aLex.escanear();
                } else {
                    indicadorDeErrores.mostrar(13, "", aLex.getLinea(), aLex.getColumna());
                }
                break;
            default:
                indicadorDeErrores.mostrar(12, "Se encontro " + aLex.getCad(), aLex.getLinea(), aLex.getColumna());
                break;
        }
    }
}
