package Compilar;

public class IndicadorDeErrores {

    public void mostrar(int cod, String cad, int linea, int columna) {
        System.err.print("ERROR en linea " + (linea + 1) + " columna " + (columna + 1) + ": ");
        switch (cod) {
            case 1:
                System.err.println("Se esperaba un punto (.)");
                break;
            case 2:
                System.err.println("Se esperaba un identificador " + cad);
                break;
            case 3:
                System.err.println("Se esperaba un ; " + cad);
                break;
            case 4:
                System.err.println("Se esperaba un numero ");
                break;
            case 6:
                System.err.println("Se esperaba un :=  " + cad);
                break;
            case 7:
                System.err.println("Se esperaba END  " + cad);
                break;
            case 8:
                System.err.println("Se esperaba THEN  " + cad);
                break;
            case 9:
                System.err.println("Se esperaba DO  " + cad);
                break;
            case 11:
                System.err.println("ERROR: Se esperaba una condicion de comparacion " + cad);    
                break;
            case 12:
                System.err.println("ERROR: Se esperaba un Id; numero o parentesis" + cad);
                break;
            case 13:
                System.err.println("Identificador duplicado: " + cad);
                break;
            case 14:
                System.err.println("Se esperaba un ) , " + cad);    
                break;
            case 15:
                System.err.println("Se esperaba un ( , " + cad);    
                break;
            case 16:
                System.err.println("Identificador duplicado: " + cad);
                break;
            case 17:
                System.err.println("Identificador no declarado: " + cad );
                break;
            case 18:
                System.err.println("Se esperaba otra operacion, se encontró: " + cad );
                break;
            case 19:
                System.err.println("Tipo incorrecto: " + cad );
                break;
            case 21:
                System.err.println("Numero " + cad + " fuera de rango");
                break;
            case 22:
                System.err.println("Identificador " + cad + " demasiado largo");
                break;
            case 23:
                System.err.println("Cadena " + cad + " demasiado larga");
                break;
            case 25:
                System.err.println("Excepcion de E/S! (" + cad + ")");
                break;
                
                
        }
        System.exit(1);
    }
}
